
CREATE DATABASE blog;

CREATE TABLE article(
    id INT AUTO_INCREMENT PRIMARY KEY,
    title VARCHAR (265),
    text TEXT,
    image VARCHAR(128),
    date DATE 
);

-- INSERT INTO article (title, text, image , date) VALUES



-- ("Porsche car",
-- "Porsche is a sports car German . The company was founded in 1931 by Ferdinand Porsche , then taken over by his son Ferry Porsche . 
-- Ferdinand Porsche is the engineer who created the first Volkswagen . The manufacturer's main factories, located in Leipzig and Zuffenhausen , 
-- Germany, had more than 17,000 employees in 2013 2 .
-- It is chronologically the tenth brand to have joined the Volkswagen AG in 2009. In 2019, its worldwide sales amounted to more than 
-- 280,000 vehicles, up from 2018.
-- Porsche AG has been part of the Volkswagen Group since 2009 and should not be confused with the listed company Porsche Automobil Holding SE 
-- (Porsche SE for short), also based in Stuttgart and majority shareholder of Volkswagen AG since 2009",
-- "https://cdn.pixabay.com/photo/2016/11/22/23/44/porsche-1851246_1280.jpg",
-- "2020-05-10"),

-- ("Ferrari car",
-- "Fiat S.p.A. acquired 50% of Ferrari in 1969 and expanded its stake to 90% in 1988. In October 2014, Fiat Chrysler Automobiles 
-- (FCA) announced its intentions to separate Ferrari S.p.A. from FCA; as of the announcement FCA owned 90% of Ferrari. 
-- The separation began in October 2015 with a restructuring that established Ferrari N.V. (a company incorporated in the Netherlands) 
-- as the new holding company of the Ferrari S.p.A. group,[8] and the subsequent sale by FCA of 10% of the shares in an IPO and concurrent 
-- listing of common shares on the New York Stock Exchange.[9] Through the remaining steps of the separation, FCA's interest in Ferrari's 
-- business was distributed to shareholders of FCA, with 10% continuing to be owned by Piero Ferrari.[10] The spin-off was completed on 
-- the 3rd of January 2016",
-- "https://cdn.pixabay.com/photo/2014/09/07/22/34/car-race-438467_960_720.jpg",
-- "2019-07-13");



-- ("Audi car",
-- "Audi is a German automotive manufacturer of luxury vehicles headquartered in Ingolstadt, Bavaria, Germany. As a subsidiary of its parent company, 
-- the Volkswagen Group, Audi produces vehicles in nine production facilities worldwide.
-- The origins of the company are complex, going back to the early 20th century and the initial enterprises (Horch and the Audiwerke) founded by engineer 
-- August Horch; and two other manufacturers (DKW and Wanderer), leading to the foundation of Auto Union in 1932. The modern Audi era began in the 1960s, 
-- when Auto Union was acquired by Volkswagen from Daimler-Benz.[10] After relaunching the Audi brand with the 1965 introduction of the Audi F103 series, 
-- Volkswagen merged Auto Union with NSU Motorenwerke in 1969, thus creating the present-day form of the company.",
-- "https://cdn.pixabay.com/photo/2016/12/03/11/47/car-1879630_1280.jpg",
-- "2019-06-05");


CREATE TABLE comments( 
   id INT AUTO_INCREMENT PRIMARY KEY, 
   id_article INT  ,
    text TEXT,
    FOREIGN KEY (id_article) REFERENCES article(id)
);

INSERT INTO comments (id_article,text) VALUES

(1, "In 1964, after a fair amount of success in motor-racing with various models including the 550 Spyder, and with the 356 needing a major re-design, 
the company launched the Porsche 911: another air-cooled, rear-engined sports car, this time with a six-cylinder . The team to lay out the 
body shell design was led by Ferry Porsche's eldest son, Ferdinand Alexander Porsche (F. A.). The design phase for the 911 caused internal problems with 
Erwin Komenda, who led the body design department until then. F. A. Porsche complained Komenda made unauthorized changes to the design. Company leader Ferry 
Porsche took his son's drawings to neighbouring chassis manufacturer Reuter. Reuter's workshop was later acquired by Porsche (so-called Werk 2). Afterward, 
Reuter became a seat manufacturer, today known as Keiper-Recaro."),

(2, "The first vehicle made with the Ferrari name was the 125 S. Only two of this small two-seat sports/racing V12 car were made. In 1949, the 166 Inter was 
introduced marking the company's significant move into the grand touring road car market. The first 166 Inter was a four-seat (2+2) berlinetta coupe with body 
work designed by Carrozzeria Touring Superleggera. Road cars quickly became the bulk of Ferrari sales. "),


(3, "The new merged company was incorporated on 1 January 1969 and was known as Audi NSU Auto Union AG, with its headquarters at NSU's Neckarsulm plant, 
and saw the emergence of Audi as a separate brand for the first time since the pre-war era. Volkswagen introduced the Audi brand to the United States for 
the 1970 model year. That same year, the mid-sized car that NSU had been working on, the K70, originally intended to slot between the rear-engined Prinz 
models and the futuristic NSU Ro 80, was instead launched as a Volkswagen.");