package co.simplon.p18.project_blog.entity;

public class Comments {
  private Integer id;
  private String text;
  public Comments() {
  }
  public Comments(String text) {
    this.text = text;
  }
  public Comments(Integer id, String text) {
    this.id = id;
    this.text = text;
  }
  public Integer getId() {
    return id;
  }
  public void setId(Integer id) {
    this.id = id;
  }
  public String getText() {
    return text;
  }
  public void setText(String text) {
    this.text = text;
  }
}
