package co.simplon.p18.project_blog.entity;

import java.sql.Date;
import java.util.List;

public class Article {
  private Integer id;
  private String title;
  private String text;
  private String image;
  private Date date;
  private List<Comments> comments;
  public Article(Integer id, String title, String text, String image) {
    this.id = id;
    this.title = title;
    this.text = text;
    this.image = image;
  }
  public Article() {
  }
  public Article(String title, String text, String image, Date date) {
    this.title = title;
    this.text = text;
    this.image = image;
    this.date = date;
  }
  public Article(Integer id, String title, String text, String image, Date date) {
    this.id = id;
    this.title = title;
    this.text = text;
    this.image = image;
    this.date = date;
  }
  public Integer getId() {
    return id;
  }
  public void setId(Integer id) {
    this.id = id;
  }
  public String getTitle() {
    return title;
  }
  public void setTitle(String title) {
    this.title = title;
  }
  public String getText() {
    return text;
  }
  public void setText(String text) {
    this.text = text;
  }
  public String getImage() {
    return image;
  }
  public void setImage(String image) {
    this.image = image;
  }
  public Date getDate() {
    return date;
  }
  public void setDate(Date date) {
    this.date = date;
  }
}
