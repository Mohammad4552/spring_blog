package co.simplon.p18.project_blog.controller;

import java.sql.Date;
import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import co.simplon.p18.project_blog.entity.Article;
import co.simplon.p18.project_blog.repository.ArticleRepository;


@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api/article")
public class ArticleController {
  @Autowired
  private ArticleRepository repo;

  @GetMapping
  public List<Article> getAll() {
    return repo.findAll();
  }

  @GetMapping ("/{id}")
  public Article getOne(@PathVariable int id){
  Article article = repo.findById(id);  
  if (article == null) {
    throw new ResponseStatusException(HttpStatus.NOT_FOUND);
  }
  return article;
}
@DeleteMapping("/{id}")
  public void deleteArticle(@PathVariable int id){
    repo.delete(id);
  }
  
  @PostMapping()
  public void createArticle(@RequestBody Article article){

    repo.create(article);
  }

}
