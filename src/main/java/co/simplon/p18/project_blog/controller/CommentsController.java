package co.simplon.p18.project_blog.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import co.simplon.p18.project_blog.entity.Comments;
import co.simplon.p18.project_blog.repository.CommentsRepository;


@RestController
@RequestMapping("/api/comments")
@CrossOrigin(origins = "http://localhost:4200")
public class CommentsController {
    @Autowired
    private CommentsRepository compo ;

    @GetMapping
    public List<Comments> getAll() {
        return compo.findAll();
    
      }

      @GetMapping ("/{id}")
  public Comments getOne(@PathVariable int id){
  Comments article = compo.findById(id);  
  if (article == null) {
    throw new ResponseStatusException(HttpStatus.NOT_FOUND);
  }
  

  return article;

}
    
}
