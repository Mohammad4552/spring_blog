package co.simplon.p18.project_blog.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import co.simplon.p18.project_blog.entity.Article;


@Repository
public class ArticleRepository {
  @Autowired

  private DataSource dataSource;

  public List<Article> findAll() {
    List<Article> list = new ArrayList<>();
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement("SELECT * FROM article");

      ResultSet rs = stmt.executeQuery();

      while (rs.next()) {
        Article article = new Article(
            rs.getInt("id"),
            rs.getString("title"),
            rs.getString("text"),
            rs.getString("image"));

        list.add(article);
      }
    } catch (SQLException e) {

      e.printStackTrace();
    }

    return list;
  }

  public Article findById(int id) {
   
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement("SELECT * FROM article WHERE id=?");

      stmt.setInt(1, id);

      ResultSet rs = stmt.executeQuery();
      

      if (rs.next()) {
        Article 
        article = new Article(
            rs.getInt("id"),
            rs.getString("title"),
            rs.getString("text"),
            rs.getString("image"));
            return article;
      }
      
    } catch (SQLException e) {

      e.printStackTrace();
    }

    return null;
  }

  public Article create(Article article) {

    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement(
          "INSERT INTO article(title,text,image) VALUES  ( ?, ?, ?)",
          PreparedStatement.RETURN_GENERATED_KEYS);

      stmt.setString(1, article.getTitle());
      stmt.setString(2, article.getText());
      stmt.setString(3, article.getImage());

      stmt.executeUpdate();

      ResultSet rs = stmt.getGeneratedKeys();
      if (rs.next()) {
        article.setId(rs.getInt(1));
      }

    } catch (SQLException e) {
      // TODO: handle exception
      e.printStackTrace();
    }
    return null;
  }

  public void delete(int id) {

    try (Connection connection = dataSource.getConnection()) {

      PreparedStatement stmt = connection.prepareStatement("DELETE FROM article WHERE id = ? ");
      stmt.setInt(1, id);

      stmt.executeUpdate();
    } catch (SQLException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
  }

  public void update(Article article) {

    try (Connection connection = dataSource.getConnection()) {

      PreparedStatement stmt = connection
          .prepareStatement("UPDATE article SET title=?, text=?, image=? WHERE id = ? ");

      stmt.setString(1, article.getTitle());
      stmt.setString(2, article.getText());
      stmt.setString(3, article.getImage());
      stmt.setInt(4, article.getId());

      stmt.executeUpdate();

      //return stmt.getUpdateCount() == 1;

    } catch (SQLException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }

  }
}
