package co.simplon.p18.project_blog.repository;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import co.simplon.p18.project_blog.entity.Comments;

@Repository
public class CommentsRepository {
    @Autowired

    private DataSource dataSource;


    public List<Comments> findAll() {
        List<Comments> list = new ArrayList<>();
        try (Connection connection = dataSource.getConnection()) {
          PreparedStatement stmt = connection.prepareStatement("SELECT * FROM comments");
    
          ResultSet rs = stmt.executeQuery();
    
          while (rs.next()) {
            Comments comments = new Comments(
                rs.getInt("id"),
                rs.getString("text"));
    
            list.add(comments);
          }
        } catch (SQLException e) {
    
          e.printStackTrace();
        }
    
        return list;
      }
    
      public Comments findById(int id) {
        Comments comments;
        try (Connection connection = dataSource.getConnection()) {
          PreparedStatement stmt = connection.prepareStatement("SELECT * FROM comments WHERE id=?");
    
          stmt.setInt(1, id);
    
          ResultSet rs = stmt.executeQuery();
    
          if (rs.next()) {
            comments = new Comments(
                rs.getInt("id"),
                rs.getString("text"));
    
                return comments;
          }
          
        } catch (SQLException e) {
    
          e.printStackTrace();
        }
    
        return null;
      }


      public Comments create(Comments comments) {

        try (Connection connection = dataSource.getConnection()) {
          PreparedStatement stmt = connection.prepareStatement(
              "INSERT INTO comments(text) VALUES  ( ?)",
              PreparedStatement.RETURN_GENERATED_KEYS);
    
          stmt.setString(1, comments.getText());
          
          stmt.executeUpdate();
    
          ResultSet rs = stmt.getGeneratedKeys();
          if (rs.next()) {
            comments.setId(rs.getInt(1));
          }
    
        } catch (SQLException e) {
          // TODO: handle exception
          e.printStackTrace();
        }
        return null;
      }

      public void update(Comments comments) {

        try (Connection connection = dataSource.getConnection()) {
    
          PreparedStatement stmt = connection
              .prepareStatement("UPDATE comments SET  text=? WHERE id = ? ");
    
          stmt.setString(1, comments.getText());
          
    
          stmt.executeUpdate();
    
          //return stmt.getUpdateCount() == 1;
    
        } catch (SQLException e) {
          // TODO Auto-generated catch block
          e.printStackTrace();
        }
    
      }
    
}
